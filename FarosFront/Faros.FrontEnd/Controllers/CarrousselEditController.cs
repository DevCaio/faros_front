﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Common;
using Faros.Common;
using Faros.Common.Constants;
using Faros.Common.Helpers;
//using Faros.FrontEnd.Models;
using Faros.FrontEnd.Proxy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Faros.FrontEnd.Controllers
{
    public class CarrousselEditController : Controller
    {

        public static List<Arquivo> lstArquivos;
        private ArquivoProxy _servicoImagem;
        private string _imagePath;
        IConfiguration _configurationOutPut;
        IHostingEnvironment _appEnviroment;

        public CarrousselEditController(IConfiguration configurationInput,
                                        IHostingEnvironment environment)

        {
            _configurationOutPut = configurationInput;
            _appEnviroment = environment;
            _imagePath = _appEnviroment.ContentRootPath + AssemblyConstants.GetPathImageByBuild(FileConstants.DEV,AssemblyConstants.PATH_IMAGE_INDEX_CARROUSSEL);
            _servicoImagem = new ArquivoProxy(_configurationOutPut.GetValue<string>("UriHost"));
        }
        public IActionResult CarrousselHome()
        {
            return View();
        }

        [HttpPost]//Colocar o post pois será passado a lista no retorno
        public IActionResult CarrousselHome(IEnumerable<Arquivo> arquivos)
        {
            return View(arquivos);
        }


        //Recebe uma lista de arquivos vindas do front usando sempre a entidade IFormFile 

        [HttpPost]
        public IActionResult UploadImagem(IList<IFormFile> arquivos)
        {

            lstArquivos = new List<Arquivo>();
            if (arquivos.Count() > 0)
            {

                int i = 0;
                MemoryStream ms;
                

                arquivos.ToList().ForEach(x =>
                {

                    //cria um memory stream para gravar os arquivos
                    ms = new MemoryStream();
                    //lê os bytes do arquivo e o copia dentro da memoryStream
                    x.OpenReadStream().CopyTo(ms);

                    lstArquivos.Add(

                        new Arquivo()
                        {
                            ContentType = x.ContentType,
                            Posicao = i++,
                            Nome = "panel_" + i.ToString(),
                            NomeSalvar = x.FileName,
                            Tamanho = "1500x500",
                            ArquivoArray = ms.ToArray()
                        });

                });

                
                ArquivoHelper.SalvarLstArquivos(lstArquivos, _imagePath);

                //Seta o path do arquivo de acordo com o caminho salvo
                SetFilesPath(lstArquivos, AssemblyConstants.GetPathImageByBuild(FileConstants.PROD,AssemblyConstants.PATH_IMAGE_INDEX_CARROUSSEL));

                _servicoImagem.PostListImagens(new List<Arquivo>(lstArquivos).Select(x=>new Arquivo() {
                    Id=x.Id,
                    ContentType=x.ContentType,
                    Nome=x.Nome,
                    NomeSalvar=x.NomeSalvar,
                    Path=x.Path,
                    Posicao=x.Posicao,
                    Tamanho=x.Tamanho
                }));


                return View("CarrousselHome", lstArquivos);

            }

            return View();
        }

        private void SetFilesPath(List<Arquivo> lstArquivos, string path)
        {
            lstArquivos
                    .ForEach(x =>
                    x.Path = ArquivoHelper.GetPathExetension(path, x.NomeSalvar, x.Nome));
        }

        [HttpGet]
        public FileStreamResult VerImagem(string nome)
        {
            var myArquivo = lstArquivos.Where(x => x.Nome == nome).FirstOrDefault();
            MemoryStream ms = new MemoryStream(myArquivo.ArquivoArray);
            return new FileStreamResult(ms, myArquivo.ContentType);
        }

        [HttpPost]
        public void PostArquivo(Arquivo arquivo)
        {
            var hehe = arquivo;
        }


    }
}