﻿using Core.Proxy;
using Faros.FrontEnd.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace Faros.FrontEnd.Proxy
{
    public class ContatoProxy : BaseProxy<ContatoDTO>
    {
        public ContatoProxy(string uri) : base(uri)
        {


        }
    }
}
